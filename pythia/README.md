# Pythia Tutorial

worksheet8300.pdf is the general worksheet for getting started with
Pythia version 8.3. Just follow the step-by-step instructions to
downlad and install, and then write and run example programs.

Contact: Nishita Desai, Leif Lonnblad
Zoom room: https://ihep-ac-cn.zoom.com.cn/j/89371879454?pwd=NDAwbWx3bzVtSGljdTF6aU9RTWk1UT09

