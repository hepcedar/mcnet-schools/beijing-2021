# Sherpa Tutorial

Contact: Davide Napoletano, Stefan Hoeche

The page for the tutorials can be found at https://www.slac.stanford.edu/~shoeche/cteq19/

The docker containers are on

  https://hub.docker.com/r/cteqschool/tutorial/tags

Tutorial itself is on Gitlab:

  git clone -b mcnet21 https://gitlab.com/shoeche/tutorials.git

Inside the tutorials just navigate to `mc/sherpa` where you can find the instructions
for the tutorials under `sherpa.pdf`.

Sherpa Manual:
https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.7.html
