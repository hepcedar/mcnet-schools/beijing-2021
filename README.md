# Tutorials for the MCnet Summer School Beijing (2021)

[WEBSITE](https://indico.ihep.ac.cn/event/11202/)

For the first two afternoons (Monday/Tuesday), choose from the following self-guided tutorials:
 * [Herwig](herwig) (Room C418, [Zoom link](https://ihep-ac-cn.zoom.com.cn/j/88471695092?pwd=UzgzUWVuNW5uMC9kV1ZkZkd5Z0JIQT09))
 * [MadGraph](madgraph) (Room C305, [Zoom link](https://ihep-ac-cn.zoom.us/j/4456457666?pwd=bkhoTDZvcGdOYjRsaTJzWXlZNHFaUT09))
 * [Pythia](pythia) (Room C623, [Zoom link](https://ihep-ac-cn.zoom.com.cn/j/89836381701?pwd=endiRFRyY0RFRy9BYzJVbkRtY3BLdz09))
 * [Sherpa](sherpa) (Room C511, [Zoom link](https://ihep-ac-cn.zoom.com.cn/j/81223546592?pwd=YVJYWHYvZnpGS0JHTWdnQ1NPeHptUT09))

For the remaining afternoons (Wednesday/Thursday), choose from the following advanced tutorials:
 * [Event generation for Heavy Ion collisions](heavy-ions) (Room C623, [Zoom link](https://ihep-ac-cn.zoom.com.cn/j/83967807373?pwd=aW91MXVDWW1VN2wrUC82c1RDKzAxUT09))
 * [Analysis prototyping and multiweights using Rivet](rivet#advanced-tutorial-analysis-prototyping-and-multiweights) (Room C511, [Zoom link](https://ihep-ac-cn.zoom.com.cn/j/87192430361?pwd=MmF5TjJmRmJFVU9YTlhrQzkvTFFBdz09))
 * [Constraining new physics models using Contur](contur) (Room C305, [Zoom link](https://ihep-ac-cn.zoom.us/j/4456457666?pwd=bkhoTDZvcGdOYjRsaTJzWXlZNHFaUT09))

## Prerequisites

Most tutorials will be based on Docker.
If you are using MacOS or Windows, you will first need to create a DockerID at https://hub.docker.com/signup

Head to https://docs.docker.com/install for installation instructions.

You can check that Docker has installed properly by running the `hello-world` Docker image

        $ docker run hello-world

Some helpful commands:

`docker run [OPTIONS] IMAGE` to run an image; 
`docker ps` to list active containers; 
`docker image ls` to list available images/apps; 
`docker attach [OPTIONS] CONTAINER` to attach to a container

When you are running inside a container, you can use `CTLR-p CTLR-q` to detach from it and leave it running. 

Please see tutorial-specific instructions as well.

