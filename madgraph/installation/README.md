## requirement on red-hat/ubuntu
 * python: version 2.7 or 3.7 or higher
 * gcc: we recomend at least version 8


## requirement on mac
 * python: version 2.7 or 3.7 or higher
 * gfortran: we do recommend to install gfortran (and NOT gcc from http://hpc.sourceforge.net/) 
 * for m1 cpu, the provided m1 optimised code does not support quadruple precision which forbids NLO computation. So we do advise to use the intel version for the moment.

## advise for windows
  * either use a virtual machine
  * use the linux subsystem (might have issue to display feynman diagrams but otherwise should be working)
   

## Downlad/install MadGraph5_aMC@NLO
  * from https://launchpad.net/mg5amcnlo
  * unpack the directory: `tar -xzpvf MG5_aMC_v3.1.1.tar.gz`
  * no compilation is needed

## First run of MG5_aMC  
  * launch the executable `./bin/mg5_aMC`
  * type `tutorial` and follow instruction

## Troubleshooting
  * MG5_aMC is using by default the "python3" executable. If you version of python3 is not recent enough it will complain about it. If you want to use python2 instead of python3, then you need to do something like `python2.7 ./bin/mg5_aMC`. If your default python version is python2.7 then you can do `python ./bin/mg5_aMC` 
     
